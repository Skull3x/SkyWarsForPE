Skywars Plugin
======
**NOTE**: this plugin may crash

## SkyWarsAPI commands

| Default command | Parameter | Description | Default Permission |
| :-----: | :-------: | :---------: | :-------: |
| /skywars | | Main SkyWars command | `OP` |
| /skywars create | `<world>` | create an arena for SkyWars | `OP` |
| /lobby | | back to lobby | `All` |
| /skywars setlobby | | Set world main lobby | `OP` |

## SkyWarsAPI Permission

| Default command | Permission |
| :-----: | :---------: |
| /skywars | `sw.command` |
| /skywars create | `sw.command.create` |
| /lobby | `sw.command.lobby` |
| /skywars setlobby | `sw.command.setlobby` |


